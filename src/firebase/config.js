import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore";


// Your web app's Firebase configuration
var firebaseConfig = {
  apiKey: "AIzaSyB4wdA3S_C5ZpIrCi_MVbh7PSbCzBbZw6s",
  authDomain: "fb-crud-react-49e51.firebaseapp.com",
  projectId: "fb-crud-react-49e51",
  storageBucket: "fb-crud-react-49e51.appspot.com",
  messagingSenderId: "350626828968",
  appId: "1:350626828968:web:0d36b24f44eda0f399289d"
  // Paste Your keys here
};
// Initialize Firebase
initializeApp(firebaseConfig);

export const db = getFirestore();
