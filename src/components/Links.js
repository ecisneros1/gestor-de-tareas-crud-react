import {React, useEffect} from "react";
import LinkForm from "./LinkForm";

import { db } from "../firebase/config.js"
import { addDoc, collection, onSnapshot } from "firebase/firestore";

const Links = () => {

    const addOrEditLink = async (linkObject) => {
        //await db.collection('links').doc().set(linkObject);
        await addDoc(collection(db, "links"), linkObject)
        console.log("new task added")
    }


    const getLinks = () => {
        onSnapshot(collection(db, "links"), (snap) => {
            console.log(snap.docs)
        }
        );
    }

    useEffect(() => { 

        getLinks();
    }, []);

    return <div>
        <LinkForm addOrEditLink={addOrEditLink} />
        <h1>Links</h1>
    </div>;
};

export default Links;
